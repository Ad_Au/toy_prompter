use amethyst;

use amethyst::{
    core::transform::TransformBundle,
    ecs::Join,
    prelude::*,
    renderer::{DisplayConfig, DrawFlat2D, Pipeline, RenderBundle, Stage},
    ui::{DrawUi, UiBundle, LineMode},
};

use amethyst::{
    assets::Loader,
    ui::{Anchor, TtfFormat, UiText, UiTransform},
};

use std::io::prelude::*;
use std::fs::File;

use amethyst::ecs::prelude::{System, WriteStorage};

struct PromptState;

impl SimpleState for PromptState {
    fn on_start(&mut self, data: StateData<'_, GameData<'_, '_>>) {
        let world = data.world;

        let font = world.read_resource::<Loader>().load(
            "Roboto-Medium.ttf",
            TtfFormat,
            Default::default(),
            (),
            &world.read_resource(),
        );

        let et = UiTransform::new(
            "Text".to_string(),
            Anchor::TopMiddle,
            0.,
            -500.,
            1.,
            512.,
            1024.,
            1,
        );

        let mut txt = String::new();
        let mut fh = File::open("assets/lorem.txt").unwrap();
        fh.read_to_string(&mut txt).unwrap();

        let mut ui_text = UiText::new(
                font.clone(),
                txt,
                [1., 1., 1., 1.],
                32.,
            );
        ui_text.line_mode = LineMode::Wrap;

        let _ = world
            .create_entity()
            .with(et)
            .with(ui_text)
            .build();
    }
}

struct ScrollSystem;

impl<'s> System<'s> for ScrollSystem {
    type SystemData = (WriteStorage<'s, UiTransform>,);

    fn run(&mut self, (mut uitransforms,): Self::SystemData) {
        for t in (&mut uitransforms).join() {
            t.local_y += 1.0;
        }
    }
}

fn main() -> amethyst::Result<()> {
    amethyst::start_logger(Default::default());

    let path = "resources/display_config.ron";
    let config = DisplayConfig::load(&path);

    let pipe = Pipeline::build().with_stage(
        Stage::with_backbuffer()
            .clear_target([0.0, 0.0, 0.0, 1.0], 1.0)
            .with_pass(DrawFlat2D::new())
            .with_pass(DrawUi::new()),
    );

    let game_data = GameDataBuilder::default()
        .with_bundle(RenderBundle::new(pipe, Some(config)))?
        .with_bundle(TransformBundle::new())?
        .with_bundle(UiBundle::<String, String>::new())?
        .with(ScrollSystem, "scroll", &[]);

    let mut game = Application::new("assets", PromptState, game_data)?;
    game.run();

    Ok(())
}
